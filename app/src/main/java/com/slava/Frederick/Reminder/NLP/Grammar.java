package com.slava.Frederick.Reminder.NLP;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by slava on 11/21/15.
 */
public class Grammar {
    //    MAIN
    private static final String HI_FREDRICK = "^(?=.*?(HI|hi|hello|Hello))(?=.*?(Frederick|frederick|fredrick|Fredrick|Doctor|doctor)).*$";
    private static final String NICE_TO_MEET_YOU_TOO = "^(?=.*?(nice|good))(?=.*?(meet|see))(?=.*?(you)).*$";
    private static final String HOW_ARE_YOU = "^(?=.*?(how))(?=.*?(are|is))(?=.*?(you|U|ya|going)).*$";
    private static final String I_AM_GOOD = "^(?=.*?(I'm|I))(?=.*?(am|feel|good|ok))(?=.*?(good|well|nice|perfect|ok)).*$";

    //    SUGAR TEST
    private static final String BEFORE_MEAL_ACTION_REGEX = "^(?=.*?(before))(?=.*?(meal)).*$";
    private static final String AFTER_MEAL_ACTION_REGEX = "^(?=.*?(after))(?=.*?(meal)).*$";
    private static final String SUGAR_LEVEL_REGEX = "^(?=.*?(my|the|a))(?=.*?(sugar|level|is))(?=.*?(\\d)).*$";
    private static final String SAVE_SUGAR_TEST = "^(?=.*?(save))(?=.*?(sugar|test|it)).*$";

    //    COMMON
    private static final String GO_TO_SUGAR_TEST = "^(?=.*?(take|go))(?=.*?())(?=.*?(blood|sugar))(?=.*?(test|level)).*$";
    private static final String GO_TO_REMINDERS = "^(?=.*?(take|go))(?=.*?())(?=.*?(reminder|reminders)).*$";
    private static final String GO_TO_PERSONAL_INFO = "^(?=.*?(take|go))(?=.*?())(?=.*?(personal|info)).*$";
    private static final String GO_TO_STATISTIC = "^(?=.*?(take|go))(?=.*?())(?=.*?(statistic)).*$";
    private static final String GO_TO_LOCATION = "^(?=.*?(take|go))(?=.*?())(?=.*?(location)).*$";
    private static final String I_FEEL_BAD = "^(?=.*?(i|I))(?=.*?(feel))(?=.*?(bad)).*$";
    private static final String YES_I_FEEL_DIZZY = "^(?=.*?(yes|yep))(?=.*?(i|I|i'm|I'm))(?=.*?(feel|am))(?=.*?(bad|dizzy)).*$";
    private static final String YES_I_FEEL_DIZZY_2 = "^(?=.*?(yes|yep))(?=.*?(i'm|I'm))(?=.*?(bad|dizzy)).*$";
    private static final String YES_I_FEEL_DIZZY_3 = "^(?=.*?(i|I))(?=.*?(feel))(?=.*?(dizzy)).*$";
    private static final String SHARE_MY_LOCATION = "^(?=.*?(send|share))(?=.*?(my|the|))(?=.*?(location)).*$";
    private static final String ERROR = "error";

    //sugar test
    private static final String GUIDE_TO_MAKE_SUGAR_TEST = "^(?=.*?(guide|instruct))(?=.*?(sugar)(?=.*?(test|level))).*$";



    private ArrayList<String> actions = new ArrayList<String>();
    NumberLexicon numbersLexicon;

    public Grammar(){
        this.BuildActionsRepository();
        this.numbersLexicon = new NumberLexicon();
    }

    private void BuildActionsRepository(){
        actions.add(0, "after meal");
        actions.add(1, "before meal");
        actions.add(2, "input sugar level");
        actions.add(3, "save");
        actions.add(4, "nice to meet you too");
        actions.add(5, "how are you");
        actions.add(6, "i am good");
        actions.add(7, "hi fredrick");
        actions.add(8, "go to sugar test");
        actions.add(9, "go to reminder");
        actions.add(10, "go to personal info");
        actions.add(11, "go to statistic");
        actions.add(12, "go to location");
        actions.add(13, "i feel bad");
        actions.add(14, "yes i feel dizzy");
        actions.add(15, "can you guide me to test sugar");
        actions.add(16, "send location");
    }

    public String InnerSystemAction(String sentence){

        if (Pattern.matches(AFTER_MEAL_ACTION_REGEX, sentence)){
            return actions.get(0);
        }
        if (Pattern.matches(BEFORE_MEAL_ACTION_REGEX, sentence)){
            return actions.get(1);
        }
        if (Pattern.matches(SUGAR_LEVEL_REGEX, sentence)){
            return actions.get(2);
        }
        if (Pattern.matches(SAVE_SUGAR_TEST, sentence)){
            return actions.get(3);
        }
        if (Pattern.matches(NICE_TO_MEET_YOU_TOO, sentence)){
            return actions.get(4);
        }
        if (Pattern.matches(HOW_ARE_YOU, sentence)){
            return actions.get(5);
        }
        if (Pattern.matches(I_AM_GOOD, sentence)){
            return actions.get(6);
        }
        if (Pattern.matches(HI_FREDRICK, sentence)){
            return actions.get(7);
        }
        if (Pattern.matches(GO_TO_SUGAR_TEST, sentence)){
            return actions.get(8);
        }
        if (Pattern.matches(GO_TO_REMINDERS, sentence)){
            return actions.get(9);
        }
        if (Pattern.matches(GO_TO_PERSONAL_INFO, sentence)){
            return actions.get(10);
        }
        if (Pattern.matches(GO_TO_STATISTIC, sentence)){
            return actions.get(11);
        }
        if (Pattern.matches(GO_TO_LOCATION, sentence)){
            return actions.get(12);
        }
        if (Pattern.matches(I_FEEL_BAD, sentence)){
            return actions.get(13);
        }
        if (Pattern.matches(YES_I_FEEL_DIZZY, sentence) || Pattern.matches(YES_I_FEEL_DIZZY_2, sentence) || Pattern.matches(YES_I_FEEL_DIZZY_3, sentence)){
            return actions.get(14);
        }
        if (Pattern.matches(GUIDE_TO_MAKE_SUGAR_TEST, sentence)){
            return actions.get(15);
        }
        if (Pattern.matches(SHARE_MY_LOCATION, sentence)){
            return actions.get(16);
        }
        return ERROR;
    }


    public String getSugarLevelFromInput(String sentence){
        String[] words = sentence.split(" ");
        // run over all words in input
        for (String word : words){
            try {
                // check if the word is integer, if it is return sugar level
                Integer sugarLevel = Integer.parseInt(word);
                return sugarLevel.toString();
            }
            // if it is not integer - check if we got sugar level as string 'ten', 'eleven' etc
            catch (NumberFormatException e){
                if (numbersLexicon.isNumberLexiconContainWord(word)){
                    return numbersLexicon.getKeyFromValue(word);
                }
            }
        }
        return  ERROR;
    }
    }

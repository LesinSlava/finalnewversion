package com.slava.Frederick.Reminder.UserProfile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blanyal.remindme.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.slava.Frederick.Reminder.AddNewMeasurement;
import com.slava.Frederick.Reminder.MainFrederickActivity;
import com.slava.Frederick.Reminder.MapsActivity;
import com.slava.Frederick.Reminder.Reminder.MainActivityReminder;
import com.slava.Frederick.Reminder.Statistic.SugarLevelStatistic;

public class UserProfile extends AppCompatActivity {


    //  For Side Menu
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;
    private Toolbar mToolbar;

    //User Data
    EditText userName;
    EditText userEmail;
    String sex;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    EditText weight;
    EditText height;
    EditText targetInFasting;
    EditText targetAfterFasting;
    private String mInsulinType;
    private TextView insulinTypeEditText;
    EditText phoneEditTxt;

    // User data from parse
    String userIdFromParse;
    String userNameFromParse;
    String userEmailFromParse;
    String sexFromParse;
    String weightFromParse;
    String heightFromParse;
    String targetInFastingFromParse;
    String targetAfterFastingFromParse;
    String insulinTypeFromParse;
    String phoneFromParse;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        initInstances();
        // Initialize Views
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        userName = (EditText) findViewById(R.id.personal_name);
        userEmail = (EditText) findViewById(R.id.personal_email);
        //Radio Btn
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        // get selected radio button from radioGroup
        int selectedId = radioSexGroup.getCheckedRadioButtonId();
        // find the radio button by returned id
        radioSexButton = (RadioButton) findViewById(selectedId);
        sex = radioSexButton.getText().toString();
        weight = (EditText) findViewById(R.id.personal_weight);
        height = (EditText) findViewById(R.id.personal_height);
        targetInFasting = (EditText) findViewById(R.id.in_fasting);
        targetAfterFasting = (EditText) findViewById(R.id.after_fasting);
        insulinTypeEditText = (TextView) findViewById(R.id.insulin_type_text_id);
        phoneEditTxt = (EditText) findViewById(R.id.phone_number);


        // Setup Toolbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Personal Info.");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Initialize default values
        mInsulinType = "Lispro (HumalogTM)";
        // Setup TextViews using reminder values
        insulinTypeEditText.setText(mInsulinType);

        //Get current user from parse
        ParseUser currentUser = ParseUser.getCurrentUser();
        userIdFromParse = currentUser.getObjectId();
        userNameFromParse = currentUser.getUsername();
        userEmailFromParse = currentUser.get("email").toString();
        sexFromParse = currentUser.get("sex").toString();
        weightFromParse = currentUser.get("weight").toString();
        heightFromParse = currentUser.get("height").toString();
        targetInFastingFromParse = currentUser.get("targetInFasting").toString();
        targetAfterFastingFromParse = currentUser.get("targetAfterFasting").toString();
        insulinTypeFromParse = currentUser.get("insulinType").toString();
        phoneFromParse = currentUser.get("phone").toString();

        userName.setText(userNameFromParse);
        userEmail.setText(userEmailFromParse);
        if(sexFromParse.equals("Male")){
            radioSexGroup.check(R.id.radioMale);
        } else if (sexFromParse.equals("Female")){
            radioSexGroup.check(R.id.radioFemale);
        } else {
            radioSexGroup.check(R.id.radioMale);
        }

        if (!weightFromParse.equals("default")){
            weight.setText(weightFromParse);
        }
        if (!heightFromParse.equals("default")){
            height.setText(heightFromParse);
        }
        if (!targetInFastingFromParse.equals("default")){
            targetInFasting.setText(targetInFastingFromParse);
        }
        if (!targetAfterFastingFromParse.equals("default")){
            targetAfterFasting.setText(targetAfterFastingFromParse);
        }
        if (!insulinTypeFromParse.equals("default")){
            insulinTypeEditText.setText(insulinTypeFromParse);
        }
        if (!phoneFromParse.equals("default")){
            phoneEditTxt.setText(phoneFromParse);
        }
    }

    public void selectInsulinType(View v){
        final String[] items = new String[5];

        items[0] = "Lispro (HumalogTM)";
        items[1] = "Glulisine (ApidraTM)";
        items[2] = "Aspart (NovologTM)";
        items[3] = "Glargine (LantusTM)";
        items[4] = "Detemir (LevemirTM)";

        // Create List Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Insulin Type");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                mInsulinType = items[item];
                insulinTypeEditText.setText(mInsulinType);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // On pressing the back button
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // Creating the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    // On clicking menu buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // On clicking the back arrow
            // Discard any changes
            case android.R.id.home:
                onBackPressed();
                return true;

            // On clicking save reminder button
            // Update reminder
            case R.id.save_profile:
                saveProfile();
                return true;

            // On clicking discard reminder button
            // Discard any changes
            case R.id.discard_profile:
//                sugarLevel.setText("Enter Sugar Level");
                Toast.makeText(getApplicationContext(), "Discarded", Toast.LENGTH_SHORT).show();
//                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //    For Side Menu
    private void initInstances() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(UserProfile.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.navigation_item_1:
                        startActivity(new Intent(UserProfile.this,MainFrederickActivity.class));
                        break;
                    case R.id.navigation_item_2:
                        startActivity(new Intent(UserProfile.this,AddNewMeasurement.class));
                        break;
                    case R.id.navigation_item_3:
                        startActivity(new Intent(UserProfile.this,MainActivityReminder.class));
                        break;
                    case R.id.navigation_item_4:
                        startActivity(new Intent(UserProfile.this, UserProfile.class));
                        break;
                    case R.id.navigation_item_5:
                        startActivity(new Intent(UserProfile.this,SugarLevelStatistic.class));
                        break;
                    case R.id.navigation_item_6:
                        startActivity(new Intent(UserProfile.this, MapsActivity.class));
                        break;
                }
                return false;
            }
        });
    }

    public void saveProfile(){
        int selectedId = radioSexGroup.getCheckedRadioButtonId();
        radioSexButton = (RadioButton) findViewById(selectedId);
        sex = radioSexButton.getText().toString();
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        // Retrieve the object by id
         query.getInBackground(userIdFromParse, new GetCallback<ParseUser>() {
             public void done(ParseUser object, ParseException e) {
                 if (e == null) {
                     object.put("username", userName.getText().toString());
                     object.put("email", userEmail.getText().toString());
                     object.put("sex", sex);
                     if (weight.getText().toString().equals("")){
                         object.put("weight", "default");
                     }else{
                         object.put("weight", weight.getText().toString());
                     }

                     if (height.getText().toString().equals("")){
                         object.put("height", "default");
                     }else{
                         object.put("height", height.getText().toString());
                     }

                     if (targetInFasting.getText().toString().equals("")){
                         object.put("targetInFasting", "default");
                     }else{
                         object.put("targetInFasting", targetInFasting.getText().toString());
                     }

                     if (targetAfterFasting.getText().toString().equals("")){
                         object.put("targetAfterFasting", "default");
                     }else{
                         object.put("targetAfterFasting", targetAfterFasting.getText().toString());
                     }

                     object.put("insulinType", insulinTypeEditText.getText().toString());

                     if (phoneEditTxt.getText().toString().equals("")){
                         object.put("phone", "default");
                     }else{
                         object.put("phone", phoneEditTxt.getText().toString());
                     }
                     object.saveInBackground();
                 }
             }
         });
    }
}

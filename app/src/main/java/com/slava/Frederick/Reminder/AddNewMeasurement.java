package com.slava.Frederick.Reminder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.blanyal.remindme.R;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.slava.Frederick.Reminder.Authentication.DataCollector;
import com.slava.Frederick.Reminder.NLP.Grammar;
import com.slava.Frederick.Reminder.NLP.Lexicon;
import com.slava.Frederick.Reminder.Reminder.MainActivityReminder;
import com.slava.Frederick.Reminder.Statistic.SugarLevelStatistic;
import com.slava.Frederick.Reminder.UserProfile.UserProfile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AddNewMeasurement extends AppCompatActivity  {
    protected static final int REQUEST_OK = 1;
    protected static final int EPSILON_SUGAR_LEVEL_AFTER_MEAL = 10;

    boolean flagToSaveSugar = false;

    Lexicon lexicon = new Lexicon();
    Grammar grammar = new Grammar();

    String sugarLevelStatus;

//    Videos
    VideoView videoIdle;
    VideoView video1;


// Speech Btn
    private FloatingActionButton speechBtn;

//  For Side Menu
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;

    private Toolbar mToolbar;
    private TextView textDate;
    private TextView textTime;
    DateFormat dateFormat;
    DateFormat timeFormat;

    private String mTestTime;
    private TextView testTimeEditText;
    public EditText sugarLevel;

    //User data from parse
    // User data from parse
    String userIdFromParse;
    String userNameFromParse;
    String userEmailFromParse;
    String sexFromParse;
    String weightFromParse;
    String heightFromParse;
    String targetInFastingFromParse;
    String targetAfterFastingFromParse;
    String insulinTypeFromParse;

    //data collector
    DataCollector globalData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_measurment);
        globalData = DataCollector.getInstance();
        initInstances();
        playIdle();

        // Initialize Views
        testTimeEditText = (TextView) findViewById(R.id.id_sugar_test_time);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        textDate = (TextView) findViewById(R.id.date_text);
        textTime = (TextView) findViewById(R.id.time_text);
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        timeFormat = new SimpleDateFormat("HH:mm");
        sugarLevel = (EditText) findViewById(R.id.edit_text_sugar_level);

        //Speech btn
        speechBtn = (FloatingActionButton) findViewById(R.id.speak_btn);


        //get current date time with Date()
        Date currentDate = new Date();
        textDate.setText(dateFormat.format(currentDate));
        Calendar cal = Calendar.getInstance();
        textTime.setText(timeFormat.format(cal.getTime()));

        // Initialize default values
        mTestTime = "Before meal";

        // Setup TextViews using reminder values
        testTimeEditText.setText(mTestTime);

        // Setup Toolbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Blood Sugar Test");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //Get current user from parse
        ParseUser currentUser = ParseUser.getCurrentUser();
        userIdFromParse = currentUser.getObjectId();
        userNameFromParse = currentUser.getUsername();
        userEmailFromParse = currentUser.get("email").toString();
        sexFromParse = currentUser.get("sex").toString();
        weightFromParse = currentUser.get("weight").toString();
        heightFromParse = currentUser.get("height").toString();
        targetInFastingFromParse = currentUser.get("targetInFasting").toString();
        targetAfterFastingFromParse = currentUser.get("targetAfterFasting").toString();
        insulinTypeFromParse = currentUser.get("insulinType").toString();

        // On clicking the floating speech button
        speechBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoIdle.pause();
                sugarLevel.addTextChangedListener(textWatcher);
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, new Long(6000));
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, "Fredrick is listening");
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(i, REQUEST_OK);
                } catch (Exception e) {
                    Toast.makeText(v.getContext(), "Error initializing speech to text engine.", Toast.LENGTH_LONG).show();
                }
            }
        });

        sugarLevel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.sugar_level_layout_id);

                if (targetAfterFastingFromParse.equals("default")) {
                    Toast.makeText(getApplicationContext(), "The test can not be saved\n" + userNameFromParse + " set your sugar target level", Toast.LENGTH_SHORT).show();
                    return true;
                }
                try {
                    flagToSaveSugar = true;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (testTimeEditText.getText().equals("After meal")) {
                            if ((Integer.parseInt(sugarLevel.getText().toString()) > Integer.parseInt(targetAfterFastingFromParse) + EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                                layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                                sugarLevelStatus = "High";
                            } else if ((Integer.parseInt(sugarLevel.getText().toString()) < Integer.parseInt(targetAfterFastingFromParse) - EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                                layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                                sugarLevelStatus = "Low";
                            } else {
                                layout.setBackgroundColor(getResources().getColor(R.color.GreenColor));
                                sugarLevelStatus = "Normal";
                            }
                        }
                        if (testTimeEditText.getText().equals("Before meal")) {
                            if ((Integer.parseInt(sugarLevel.getText().toString()) > Integer.parseInt(targetInFastingFromParse) + EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                                layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                                sugarLevelStatus = "High";
                            } else if ((Integer.parseInt(sugarLevel.getText().toString()) < Integer.parseInt(targetInFastingFromParse) - EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                                layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                                sugarLevelStatus = "Low";
                            } else {
                                layout.setBackgroundColor(getResources().getColor(R.color.GreenColor));
                                sugarLevelStatus = "Normal";
                            }
                        }
                    }
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Invalid sugar level", Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
        });
    }

    // On clicking repeat type button
    public void selectTestTime(View v){
        final String[] items = new String[2];

        items[0] = "After meal";
        items[1] = "Before meal";

        // Create List Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Test Time");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                mTestTime = items[item];
                testTimeEditText.setText(mTestTime);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // On pressing the back button
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // Creating the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sugar_test, menu);
        return true;
    }

    // On clicking menu buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // On clicking the back arrow
            // Discard any changes
            case android.R.id.home:
                onBackPressed();
                return true;

            // On clicking save reminder button
            // Update reminder
            case R.id.save_sugar_test:
                if (flagToSaveSugar){
                    saveSugarTest();
                }
                else {
                    Toast.makeText(getApplicationContext(), "For unexpected reason the test can not be saved", Toast.LENGTH_SHORT).show();
                }

                return true;

            // On clicking discard reminder button
            // Discard any changes
            case R.id.drop_sugar_test:
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.sugar_level_layout_id);
                layout.setBackgroundColor(getResources().getColor(R.color.primary_layout_background));

                sugarLevel.setText("Enter Sugar Level");
                Toast.makeText(getApplicationContext(), "Discarded", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

// Save sugar test in parse
    private void saveSugarTest(){

        String sugarLevelAsString = sugarLevel.getText().toString();
        if (sugarLevelAsString == "Enter Sugar Level"){
            Toast.makeText(getApplicationContext(), "Input sugar level", Toast.LENGTH_SHORT).show();
            return;
        }

        try{
            int sugarLevelAsInt = Integer.parseInt(sugarLevelAsString);

            if (sugarLevelAsInt < 20 || sugarLevelAsInt > 150){
                throw new Exception();
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Invalid sugar level", Toast.LENGTH_SHORT).show();
            return;
        }


        ParseObject sugarTest = new ParseObject("SugarTestLevel");
        sugarTest.put("date", textDate.getText().toString());
        sugarTest.put("time", textTime.getText().toString());
        sugarTest.put("sugarTestTime", testTimeEditText.getText().toString());
        sugarTest.put("sugarLevel", sugarLevel.getText().toString());
        sugarTest.put("status", sugarLevelStatus);
        sugarTest.put("userID", globalData.getUserID());
        sugarTest.saveInBackground();
        Toast.makeText(getApplicationContext(), "The sugar level was saved", Toast.LENGTH_SHORT).show();

    }

//    For Side Menu
    private void initInstances() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(AddNewMeasurement.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.navigation_item_1:
                        startActivity(new Intent(AddNewMeasurement.this, MainFrederickActivity.class));
                        break;
                    case R.id.navigation_item_2:
                        startActivity(new Intent(AddNewMeasurement.this, AddNewMeasurement.class));
                        break;
                    case R.id.navigation_item_3:
                        startActivity(new Intent(AddNewMeasurement.this, MainActivityReminder.class));
                        break;
                    case R.id.navigation_item_4:
                        startActivity(new Intent(AddNewMeasurement.this, UserProfile.class));
                        break;
                    case R.id.navigation_item_5:
                        startActivity(new Intent(AddNewMeasurement.this, SugarLevelStatistic.class));
                        break;
                    case R.id.navigation_item_6:
                        startActivity(new Intent(AddNewMeasurement.this, MapsActivity.class));
                        break;
                }
                return false;
            }
        });
    }

    @Override
    //Check sugar level
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OK && resultCode == RESULT_OK) {
            ArrayList<String> thingsYouSaid = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String bestEffort = thingsYouSaid.get(0);
            Toast.makeText(getApplicationContext(), bestEffort, Toast.LENGTH_LONG).show();

            //Check if the sentence contain at least one word from the lexicon, if not we cannot handle it
            String action;
            if (lexicon.sanityTestOnSentence(bestEffort)) {
                action = grammar.InnerSystemAction(bestEffort);

            }else{
                action = "error";
            }
            switch (action) {
                case "after meal": {
                    testTimeEditText.setText("After meal");
                    break;
                }
                case "before meal": {
                    testTimeEditText.setText("Before meal");
                    break;
                }
                case "input sugar level": {
                    String sugarLevelFromUser = grammar.getSugarLevelFromInput(bestEffort);
                    sugarLevel.setText(sugarLevelFromUser);
                    break;
                }
                case "can you guide me to test sugar": {
                    this.playVideo(R.raw.guide_ti_test_sugar);
                    break;
                }
                case "save": {
                    this.playVideo(R.raw.save_test);
                    saveSugarTest();
                    break;
                }
                case "how are you": {
                    this.playVideo(R.raw.i_feel_good_how_are_you);
                    break;
                }
                case "i am good": {
                    this.playVideo(R.raw.glad_to_hear_you_are_ok);
                    break;
                }
                case "hi fredrick": {
                    int x=(Math.random()<0.5)?0:1;
                    String SrcPath;
                    if (x==0){
                        this.playVideo(R.raw.hi_what_can_i_do_for_you);
                    }else{
                        this.playVideo(R.raw.anything_i_can_do_for_you);
                    }
                    break;
                }
                case "go to sugar test": {
                    this.playVideoAndGoToSugarTest();
                    break;
                }
                case "go to reminder": {
                    this.playVideoAndGoToReminder();
                    break;
                }
                case "go to personal info": {
                    this.playVideoAndGoToPersonalInfo();
                    break;
                }
                case "go to statistic": {
                    this.playVideoAndGoToStatistic();
                    break;
                }
                case "go to location": {
                    this.playVideoAndGoToShareLocation();
                    break;
                }
                case "i feel bad": {
                    this.playVideo(R.raw.sorry_to_hear_you_bad_are_you_dzzy);
                    break;
                }
                case "yes i feel dizzy": {
                    this.letsCheckYourSugatAndGoToSugarMeasurement();
                    break;
                }

                case "error": {
                    this.playVideo(R.raw.repeat_please);
                    break;
                }
            }
        }
    }

    public void playIdle(){
        videoIdle = (VideoView)findViewById(R.id.videoViewAddNewMeasurement);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.idle;
        Uri vidUri = Uri.parse(SrcPath);
        videoIdle.setVideoURI(vidUri);
        videoIdle.start();
        videoIdle.requestFocus();
        videoIdle.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
    }

    public void playVideo(int id){
        video1 = (VideoView) findViewById(R.id.videoViewAddNewMeasurement);
        String SrcPath = "android.resource://com.blanyal.remindly/" + id;
        Uri vidUri = Uri.parse(SrcPath);
        video1.setVideoURI(vidUri);
        video1.start();
        video1.requestFocus();

        video1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video1.stopPlayback();
                playIdle();
            }
        });
    }


    protected TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) {
            RelativeLayout layout = (RelativeLayout) findViewById(R.id.sugar_level_layout_id);
            if (testTimeEditText.getText().equals("After meal")) {
                if ((Integer.parseInt(sugarLevel.getText().toString()) > Integer.parseInt(targetAfterFastingFromParse) + EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                    layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                    sugarLevelStatus = "High";
                    playVideo(R.raw.sugar_level_to_high);
                } else if ((Integer.parseInt(sugarLevel.getText().toString()) < Integer.parseInt(targetAfterFastingFromParse) - EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                    layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                    sugarLevelStatus = "Low";
                    playVideo(R.raw.sugar_level_to_low);
                } else {
                    layout.setBackgroundColor(getResources().getColor(R.color.GreenColor));
                    sugarLevelStatus = "Normal";
                    playVideo(R.raw.sugar_level_normal);
                }
            }
            if (testTimeEditText.getText().equals("Before meal")) {
                if ((Integer.parseInt(sugarLevel.getText().toString()) > Integer.parseInt(targetInFastingFromParse) + EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                    layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                    sugarLevelStatus = "High";
                    playVideo(R.raw.sugar_level_to_high);
                } else if ((Integer.parseInt(sugarLevel.getText().toString()) < Integer.parseInt(targetInFastingFromParse) - EPSILON_SUGAR_LEVEL_AFTER_MEAL)) {
                    layout.setBackgroundColor(getResources().getColor(R.color.PrimaryColor));
                    sugarLevelStatus = "Low";
                    playVideo(R.raw.sugar_level_to_low);
                } else {
                    layout.setBackgroundColor(getResources().getColor(R.color.GreenColor));
                    sugarLevelStatus = "Normal";
                    playVideo(R.raw.sugar_level_normal);
                }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // your logic here
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // your logic here
        }
    };

    public void playVideoAndGoToPersonalInfo(){
        video1 = (VideoView) findViewById(R.id.videoViewAddNewMeasurement);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_take_you_there;
        Uri vidUri = Uri.parse(SrcPath);
        video1.setVideoURI(vidUri);
        video1.start();
        video1.requestFocus();

        video1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video1.stopPlayback();
                startActivity(new Intent(AddNewMeasurement.this, UserProfile.class));
            }
        });
    }

    public void playVideoAndGoToStatistic(){
        video1 = (VideoView) findViewById(R.id.videoViewAddNewMeasurement);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_do_it;
        Uri vidUri = Uri.parse(SrcPath);
        video1.setVideoURI(vidUri);
        video1.start();
        video1.requestFocus();

        video1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video1.stopPlayback();
                startActivity(new Intent(AddNewMeasurement.this, SugarLevelStatistic.class));
            }
        });
    }

    public void letsCheckYourSugatAndGoToSugarMeasurement(){
        video1 = (VideoView) findViewById(R.id.videoViewAddNewMeasurement);
        int x=(Math.random()<0.5)?0:1;
        String SrcPath;
        if (x==0){
            SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.lets_check_your_sugar_level;
        }else{
            SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.not_fun_lets_check_the_sugar;
        }
        Uri vidUri = Uri.parse(SrcPath);
        video1.setVideoURI(vidUri);
        video1.start();
        video1.requestFocus();

        video1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video1.stopPlayback();
            }
        });
    }

    public void playVideoAndGoToShareLocation(){
        video1 = (VideoView) findViewById(R.id.videoViewAddNewMeasurement);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_do_it;
        Uri vidUri = Uri.parse(SrcPath);
        video1.setVideoURI(vidUri);
        video1.start();
        video1.requestFocus();

        video1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video1.stopPlayback();
                startActivity(new Intent(AddNewMeasurement.this, MapsActivity.class));
            }
        });
    }

    public void playVideoAndGoToSugarTest(){
        video1 = (VideoView) findViewById(R.id.videoViewAddNewMeasurement);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_take_you_there;
        Uri vidUri = Uri.parse(SrcPath);
        video1.setVideoURI(vidUri);
        video1.start();
        video1.requestFocus();

        video1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video1.stopPlayback();
                startActivity(new Intent(AddNewMeasurement.this, AddNewMeasurement.class));
            }
        });
    }

    public void playVideoAndGoToReminder(){
        video1 = (VideoView) findViewById(R.id.videoViewAddNewMeasurement);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_do_it;
        Uri vidUri = Uri.parse(SrcPath);
        video1.setVideoURI(vidUri);
        video1.start();
        video1.requestFocus();

        video1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video1.stopPlayback();
                startActivity(new Intent(AddNewMeasurement.this, MainActivityReminder.class));
            }
        });
    }

}

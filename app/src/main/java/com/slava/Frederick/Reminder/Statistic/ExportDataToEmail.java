package com.slava.Frederick.Reminder.Statistic;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.blanyal.remindme.R;
import com.parse.ParseObject;
import com.slava.Frederick.Reminder.AddNewMeasurement;
import com.slava.Frederick.Reminder.Authentication.DataCollector;
import com.slava.Frederick.Reminder.MainFrederickActivity;
import com.slava.Frederick.Reminder.Reminder.MainActivityReminder;
import com.slava.Frederick.Reminder.UserProfile.UserProfile;

import java.util.List;

public class ExportDataToEmail extends AppCompatActivity {
    //toolbar
    // For side menu
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;
    private Toolbar mToolbar;
    EditText emailText;
    EditText ccText;

    List<ParseObject> tests;
    DataCollector globalData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expotr_data_to_email);
        initInstances();

        globalData = DataCollector.getInstance();
        tests = globalData.getTests();


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        // Setup Toolbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Export Statistic");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        emailText = (EditText)findViewById(R.id.email);
        ccText = (EditText)findViewById(R.id.cc_email);
    }

    // Creating the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mail, menu);
        return true;
    }

    // On clicking menu buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // On clicking the back arrow
            // Discard any changes
            case android.R.id.home:
                onBackPressed();
                return true;

            // On clicking save reminder button
            case R.id.send_email:
                sendEmail();
                return true;

            // On clicking discard reminder button
            // Discard any changes
            case R.id.discard_email:
                emailText.setText("");
                ccText.setText("");
                Toast.makeText(getApplicationContext(), "Discarded", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initInstances() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(ExportDataToEmail.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.navigation_item_1:
                        startActivity(new Intent(ExportDataToEmail.this, MainFrederickActivity.class));
                        break;
                    case R.id.navigation_item_2:
                        startActivity(new Intent(ExportDataToEmail.this, AddNewMeasurement.class));
                        break;
                    case R.id.navigation_item_3:
                        startActivity(new Intent(ExportDataToEmail.this, MainActivityReminder.class));
                        break;
                    case R.id.navigation_item_4:
                        startActivity(new Intent(ExportDataToEmail.this, UserProfile.class));
                        break;
                    case R.id.navigation_item_5:
                        startActivity(new Intent(ExportDataToEmail.this, SugarLevelStatistic.class));
                        break;
                }
                return false;
            }
        });
    }

    protected void sendEmail() {

        Intent emailIntent2 = new Intent(android.content.Intent.ACTION_SEND);
        String[] recipients = new String[]{emailText.getText().toString()};
        emailIntent2.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
        String[] ccs = ccText.getText().toString().split(",");
        emailIntent2.putExtra(Intent.EXTRA_CC, ccs);
        emailIntent2.putExtra(android.content.Intent.EXTRA_SUBJECT, "Blood Sugar Statistics");

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<b><i> Sugar statistic for last 50 days </i></b>");
        stringBuilder.append("<html>");
        stringBuilder.append("<body>");

        for (ParseObject test : tests){
            stringBuilder.append("<div><b>Date and time: </b>" + test.get("date") + " " + test.get("time") + "</div>");
            stringBuilder.append("<div><b>Sugar level: </b>" + "<strong>" + test.get("sugarLevel") + " mg/l </strong></div>");
            stringBuilder.append("<div><b>Tested: </b>" + test.get("sugarTestTime") + "</div>");
            if(test.get("status").equals("Normal")){
                stringBuilder.append("<div><b>Status: </b>" + "<font color='#00FF00'>" + test.get("status") + "</font></div>");
            }else{
                stringBuilder.append("<div><b>Status: </b>" + "<font color='#FF0000'>" + test.get("status") + "</font></div>");
            }
            stringBuilder.append("<div> ------------------------------------------------------ </div>");
        }

        stringBuilder.append("<div><i> This email was sent by Frederick, Virtual medical assistant</i></div>");
        stringBuilder.append("</html>");
        stringBuilder.append("</body>");

        emailIntent2.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(stringBuilder.toString()));
        emailIntent2.setType("text/html");
        startActivity(Intent.createChooser(emailIntent2, "Send mail client :"));
        finish();
    }

}

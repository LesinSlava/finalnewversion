package com.slava.Frederick.Reminder.Authentication;

import android.app.Application;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by slava on 4/5/16.
 */
public class DataCollector extends Application {
    private static DataCollector instance;
    List<ParseObject> tests;
    List<ParseObject> todayTests;
    String userID;
    ParseUser currentUser;
    List<ParseObject> lastSugarTest;
    String phone;
    String firstLogin;

    public DataCollector() {}

    public List<ParseObject> getTodayTests() {
        return todayTests;
    }

    public void setTodayTests(List<ParseObject> todayTests) {
        this.todayTests = todayTests;
    }

    public void setTests(List<ParseObject> tests) {
        this.tests = tests;
    }

    public List<ParseObject> getTests() {
        return tests;
    }

    public ParseObject lastSugarTestInDataCollector(){
        if (this.lastSugarTest.size() == 0){
            return null;
        }
        return this.lastSugarTest.get(0);
    }

    public String phoneNumber() {
        this.getUserFromParse();
        return this.phone;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public static synchronized DataCollector getInstance(){
        if(instance==null){
            instance=new DataCollector();
        }
        return instance;
    }

    public void getLastSugarTest(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("SugarTestLevel");
        query.orderByDescending("updatedAt");
        query.whereEqualTo("userID", currentUser.getObjectId());

        try {
            this.lastSugarTest = query.find();
        }catch (ParseException e){}
    }

    public void getUserFromParse(){
        this.currentUser = ParseUser.getCurrentUser();
        this.setUserID(currentUser.getObjectId());
        this.phone = currentUser.get("phone").toString();
        this.firstLogin = currentUser.get("firstLogin").toString();
    }

    public void getAllSugarTestsFromParse(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("SugarTestLevel");
        query.whereEqualTo("userID", this.currentUser.getObjectId());
        try {
            this.tests = query.find();
        }catch (ParseException e){}
    }

    public void getTodaySugarTestsFromParse(){
        try{
            Calendar cal = new GregorianCalendar();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);

            // start of today
            Date today = cal.getTime();

            cal.add(Calendar.DAY_OF_MONTH, 1); // add one day to get start of tomorrow

            // start of tomorrow
            Date tomorrow = cal.getTime();

            ParseQuery<ParseObject> queryTestsFromToday = ParseQuery.getQuery("SugarTestLevel");
            queryTestsFromToday.whereEqualTo("userID", currentUser.getObjectId());
            queryTestsFromToday.whereGreaterThanOrEqualTo("createdAt", today);
            queryTestsFromToday.whereLessThan("createdAt", tomorrow);
            this.todayTests = queryTestsFromToday.find();

        } catch (ParseException e){}
    }

    public String isFirstLogin(){
        return this.firstLogin;
    }
    public void setFirstLogin(final String isFirst){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.getInBackground(this.userID, new GetCallback<ParseUser>() {
            public void done(ParseUser object, ParseException e) {
                if (e == null) {
                    object.put("firstLogin", isFirst);
                    object.saveInBackground();
                }
            }
        });
        this.firstLogin=isFirst;
    }
}

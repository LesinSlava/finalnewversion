package com.slava.Frederick.Reminder;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.blanyal.remindme.R;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.parse.ParseObject;
import com.slava.Frederick.Reminder.Authentication.DataCollector;
import com.slava.Frederick.Reminder.NLP.Grammar;
import com.slava.Frederick.Reminder.NLP.Lexicon;
import com.slava.Frederick.Reminder.Reminder.MainActivityReminder;
import com.slava.Frederick.Reminder.Statistic.SugarLevelStatistic;
import com.slava.Frederick.Reminder.UserProfile.UserProfile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainFrederickActivity extends AppCompatActivity {
    Lexicon lexicon = new Lexicon();
    Grammar grammar = new Grammar();
    // Speech Btn
    private FloatingActionButton speechBtn;

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;
    private Toolbar mToolbar;
    DataCollector globalData;
    ParseObject lastTest;

    TextView time;
    TextView day;
    TextView sugarLevel;
    String status;

    ImageView levelImg;

    protected static final int REQUEST_OK = 1;
    VideoView videoIdle;
    VideoView video;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_frederick);

        time = (TextView)findViewById(R.id.date_text);
        day = (TextView)findViewById(R.id.time_text);
        sugarLevel = (TextView)findViewById(R.id.sugar_level);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        levelImg = (ImageView) findViewById(R.id.sugar_level_image);
        // Setup Toolbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Frederick");
        globalData = DataCollector.getInstance();
        globalData.getLastSugarTest();
        this.lastTest = globalData.lastSugarTestInDataCollector();
        //Speech btn
        speechBtn = (FloatingActionButton) findViewById(R.id.speak_btn);

        // On clicking the floating speech button
        speechBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoIdle.pause();
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, new Long(6000));
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, "Fredrick is listening");
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(i, REQUEST_OK);
                } catch (Exception e) {
                    Toast.makeText(v.getContext(), "Error initializing speech to text engine.", Toast.LENGTH_LONG).show();
                }
            }
        });

        if(this.lastTest == null){
            time.setText("time");
            day.setText("date");
            sugarLevel.setText("0");
            status = "status";
        }else {
            time.setText(this.lastTest.get("time").toString());
            day.setText(this.lastTest.get("date").toString());
            sugarLevel.setText(this.lastTest.get("sugarLevel").toString());
            status = this.lastTest.get("status").toString();
        }

        if (status.equals("Low") || status.equals("High")){
            int color = Color.parseColor("#EF5350");
            levelImg.setColorFilter(color);
        }else{
            int color = Color.parseColor("#00E676");
            levelImg.setColorFilter(color);
        }

        initInstances();
        if(globalData.isFirstLogin().equals("true")) {
            playVideo(R.raw.hi_iam_fredrick);
            globalData.setFirstLogin("false");
        }else{
            playIdle();
        }

        //Check when was the last sugar test, if more than 3 hour over tell user to check sugar level
        try {
            String[] lastTestTime = this.time.getText().toString().split(":");
            int lastTestHour = Integer.parseInt(lastTestTime[0]);
            Calendar cal = Calendar.getInstance();
            DateFormat timeFormat = new SimpleDateFormat("HH:mm");
            String[] currentTime = (timeFormat.format(cal.getTime())).split(":");
            int currentTimeLessHour = Integer.parseInt(currentTime[0]) - 3;

            if (lastTestHour < currentTimeLessHour) {
                playVideo(R.raw.long_time_since_last_test);
            }
        }catch (NumberFormatException e){

        }
    }


    public void playIdle(){
        videoIdle = (VideoView)findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.idle;
        Uri vidUri = Uri.parse(SrcPath);
        videoIdle.setVideoURI(vidUri);
        videoIdle.start();
        videoIdle.requestFocus();
        videoIdle.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
    }

    public void playVideo(int id){
        video = (VideoView) findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + id;
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video.stopPlayback();
                playIdle();
            }
        });
    }

    public void playVideoAndShareLocation(){
        video = (VideoView) findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_do_it;
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        sendMyLocation();
    }

    public void sendMyLocation(){
        double[] location = getCurrentLocation();
        String phoneNumber = globalData.phoneNumber();
        if(phoneNumber.equals("default") || phoneNumber.equals(null)){
            Toast.makeText(getApplicationContext(), "Set phone number in personal profile", Toast.LENGTH_SHORT).show();
            return;
        }
        SmsManager smsManager = SmsManager.getDefault();
        StringBuffer smsBody = new StringBuffer();
        smsBody.append("I need help my location is:\n");
        smsBody.append("http://maps.google.com/?q=");
        smsBody.append(location[1]);
        smsBody.append(",");
        smsBody.append(location[0]);
        smsManager.sendTextMessage(phoneNumber, null, smsBody.toString(), null, null);
        Toast.makeText(getApplicationContext(), "Your location was shared with \n" + phoneNumber, Toast.LENGTH_SHORT).show();

    }

    public double[] getCurrentLocation(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        return new double[] {longitude, latitude};
    }

    public void playVideoAndGoToSugarTest(){
        video = (VideoView) findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_take_you_there;
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video.stopPlayback();
                startActivity(new Intent(MainFrederickActivity.this, AddNewMeasurement.class));
            }
        });
    }

    public void playVideoAndGoToReminder(){
        video = (VideoView) findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_do_it;
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video.stopPlayback();
                startActivity(new Intent(MainFrederickActivity.this, MainActivityReminder.class));
            }
        });
    }

    public void playVideoAndGoToPersonalInfo(){
        video = (VideoView) findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_take_you_there;
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video.stopPlayback();
                startActivity(new Intent(MainFrederickActivity.this, UserProfile.class));
            }
        });
    }

    public void playVideoAndGoToStatistic(){
        video = (VideoView) findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_do_it;
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video.stopPlayback();
                startActivity(new Intent(MainFrederickActivity.this, SugarLevelStatistic.class));
            }
        });
    }

    public void letsCheckYourSugatAndGoToSugarMeasurement(){
        video = (VideoView) findViewById(R.id.videoViewMain);
        int x=(Math.random()<0.5)?0:1;
        String SrcPath;
        if (x==0){
             SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.lets_check_your_sugar_level;
        }else{
            SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.not_fun_lets_check_the_sugar;
        }
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video.stopPlayback();
                startActivity(new Intent(MainFrederickActivity.this, AddNewMeasurement.class));
            }
        });
    }

    public void playVideoAndGoToShareLocation(){
        video = (VideoView) findViewById(R.id.videoViewMain);
        String SrcPath = "android.resource://com.blanyal.remindly/" + R.raw.i_will_do_it;
        Uri vidUri = Uri.parse(SrcPath);
        video.setVideoURI(vidUri);
        video.start();
        video.requestFocus();

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                video.stopPlayback();
                startActivity(new Intent(MainFrederickActivity.this, MapsActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OK && resultCode == RESULT_OK) {
            ArrayList<String> thingsYouSaid = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String bestEffort = thingsYouSaid.get(0);
            Toast.makeText(getApplicationContext(), bestEffort, Toast.LENGTH_LONG).show();


            //Check if the sentence contain at least one word from the lexicon, if not we cannot handle it
            String action;
            if (lexicon.sanityTestOnSentence(bestEffort)) {
                action = grammar.InnerSystemAction(bestEffort);

            }else{
                action = "error";
            }
            switch (action) {
                case "nice to meet you too": {
                    this.playVideo(R.raw.nice_to_meet_you_too);
                    break;
                }
                case "how are you": {
                    this.playVideo(R.raw.i_feel_good_how_are_you);
                    break;
                }
                case "i am good": {
                    this.playVideo(R.raw.glad_to_hear_you_are_ok);
                    break;
                }
                case "hi fredrick": {
                    int x=(Math.random()<0.5)?0:1;
                    String SrcPath;
                    if (x==0){
                        this.playVideo(R.raw.hi_what_can_i_do_for_you);
                    }else{
                        this.playVideo(R.raw.anything_i_can_do_for_you);
                    }
                    break;
                }
                case "go to sugar test": {
                    this.playVideoAndGoToSugarTest();
                    break;
                }
                case "go to reminder": {
                    this.playVideoAndGoToReminder();
                    break;
                }
                case "go to personal info": {
                    this.playVideoAndGoToPersonalInfo();
                    break;
                }
                case "go to statistic": {
                    this.playVideoAndGoToStatistic();
                    break;
                }
                case "go to location": {
                    this.playVideoAndGoToShareLocation();
                    break;
                }
                case "i feel bad": {
                    this.playVideo(R.raw.sorry_to_hear_you_bad_are_you_dzzy);
                    break;
                }
                case "yes i feel dizzy": {
                    this.letsCheckYourSugatAndGoToSugarMeasurement();
                    break;
                }

                case "error": {
                    this.playVideo(R.raw.repeat_please);
                    break;
                }
                case "send location": {
                    this.playVideoAndShareLocation();
                    break;
                }
            }
        }
    }

    private void initInstances() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(MainFrederickActivity.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.navigation_item_1:
                        startActivity(new Intent(MainFrederickActivity.this, MainFrederickActivity.class));
                        break;
                    case R.id.navigation_item_2:
                        startActivity(new Intent(MainFrederickActivity.this, AddNewMeasurement.class));
                        break;
                    case R.id.navigation_item_3:
                        startActivity(new Intent(MainFrederickActivity.this, MainActivityReminder.class));
                        break;
                    case R.id.navigation_item_4:
                        startActivity(new Intent(MainFrederickActivity.this, UserProfile.class));
                        break;
                    case R.id.navigation_item_5:
                        startActivity(new Intent(MainFrederickActivity.this, SugarLevelStatistic.class));
                        break;
                    case R.id.navigation_item_6:
                        startActivity(new Intent(MainFrederickActivity.this, MapsActivity.class));
                        break;
                }
                return false;
            }
        });

    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    // Creating the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_sugar_test, menu);
        return true;
    }

    // On clicking menu buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // On clicking the back arrow
            // Discard any changes
            case android.R.id.home:
                onBackPressed();
                return true;

            // On clicking save reminder button
            // Update reminder
            case R.id.save_sugar_test:
//                saveSugarTest();
                return true;

            // On clicking discard reminder button
            // Discard any changes
            case R.id.drop_sugar_test:
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.sugar_level_layout_id);
                layout.setBackgroundColor(getResources().getColor(R.color.primary_layout_background));

//                sugarLevel.setText("Enter Sugar Level");
                Toast.makeText(getApplicationContext(), "Discarded", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


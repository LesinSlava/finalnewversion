package com.slava.Frederick.Reminder.NLP;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by slava on 3/26/16.
 */
public class NumberLexicon {
    Map<String, String> dictOfNumbers = new HashMap<>();

    public NumberLexicon() {
        buildNumberLexicon();
    }

    private void buildNumberLexicon(){
        // NUMBERS
        dictOfNumbers.put("0", "zero");
        dictOfNumbers.put("1", "one");
        dictOfNumbers.put("2", "two");
        dictOfNumbers.put("3", "three");
        dictOfNumbers.put("4", "four");
        dictOfNumbers.put("5", "five");
        dictOfNumbers.put("6", "six");
        dictOfNumbers.put("7", "seven");
        dictOfNumbers.put("8", "eight");
        dictOfNumbers.put("9", "nine");
        dictOfNumbers.put("10", "ten");
        dictOfNumbers.put("11", "eleven");
        dictOfNumbers.put("12", "twelve");
        dictOfNumbers.put("13", "thirteen");
        dictOfNumbers.put("14", "fourteen");
        dictOfNumbers.put("15", "fifteen");
        dictOfNumbers.put("16", "sixteen");
        dictOfNumbers.put("17", "seventeen");
        dictOfNumbers.put("18", "eighteen");
        dictOfNumbers.put("19", "nineteen");
        dictOfNumbers.put("20", "twenty");
    }

    // Check if the word in lexicon
    public boolean isNumberLexiconContainWord(String word) {
        return dictOfNumbers.containsKey(word) || dictOfNumbers.containsValue(word) ;
    }

    public String getKeyFromValue(String value){
        for (String num : this.dictOfNumbers.keySet()) {
            if (this.dictOfNumbers.get(num).equals(value)) {
                return num;
            }
        }
        return "error";
    }

}

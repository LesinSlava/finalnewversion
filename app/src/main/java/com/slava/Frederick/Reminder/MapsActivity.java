package com.slava.Frederick.Reminder;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.blanyal.remindme.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.slava.Frederick.Reminder.Authentication.DataCollector;
import com.slava.Frederick.Reminder.Reminder.MainActivityReminder;
import com.slava.Frederick.Reminder.Statistic.SugarLevelStatistic;
import com.slava.Frederick.Reminder.UserProfile.UserProfile;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Toolbar mToolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;

    //data collector
    DataCollector globalData;
    String phone;
    TextView phoneTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        globalData = DataCollector.getInstance();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        phoneTextView = (TextView)findViewById(R.id.phone_nubler_textview);
        phone = globalData.phoneNumber();
        if(phone.equals("default")){
            phoneTextView.setText("not provided");
        }else{
            phoneTextView.setText(phone);
        }

        // Setup Toolbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Share My location");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initInstances();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        double[] location = getCurrentLocation();
        // Add a marker in current and move the camera
        LatLng currentLocation = new LatLng(location[1], location[0]);
        mMap.addMarker(new MarkerOptions().position(currentLocation).title("I am here"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 14));
    }

    public double[] getCurrentLocation(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        return new double[] {longitude, latitude};
    }

    private void initInstances() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(MapsActivity.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.navigation_item_1:
                        startActivity(new Intent(MapsActivity.this, MainFrederickActivity.class));
                        break;
                    case R.id.navigation_item_2:
                        startActivity(new Intent(MapsActivity.this, AddNewMeasurement.class));
                        break;
                    case R.id.navigation_item_3:
                        startActivity(new Intent(MapsActivity.this, MainActivityReminder.class));
                        break;
                    case R.id.navigation_item_4:
                        startActivity(new Intent(MapsActivity.this, UserProfile.class));
                        break;
                    case R.id.navigation_item_5:
                        startActivity(new Intent(MapsActivity.this, SugarLevelStatistic.class));
                        break;
                    case R.id.navigation_item_6:
                        startActivity(new Intent(MapsActivity.this, MapsActivity.class));
                        break;
                }
                return false;
            }
        });
    }

    // Creating the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share_location, menu);
        return true;
    }

    // On clicking menu buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // On clicking the back arrow
            // Discard any changes
            case android.R.id.home:
                onBackPressed();
                return true;

            // On clicking send location
            // Update reminder
            case R.id.share_location:
                sendMyLocation();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sendMyLocation(){
        double[] location = getCurrentLocation();
        String phoneNumber = globalData.phoneNumber();
        if(phoneNumber.equals("default") || phoneNumber.equals(null)){
            Toast.makeText(getApplicationContext(), "Set phone number in personal profile", Toast.LENGTH_SHORT).show();
            return;
        }
        SmsManager smsManager = SmsManager.getDefault();
        StringBuffer smsBody = new StringBuffer();
        smsBody.append("I need help my location is:\n");
        smsBody.append("http://maps.google.com/?q=");
        smsBody.append(location[1]);
        smsBody.append(",");
        smsBody.append(location[0]);
        smsManager.sendTextMessage(phoneNumber, null, smsBody.toString(), null, null);
        Toast.makeText(getApplicationContext(), "Your location was shared with \n" + phoneNumber, Toast.LENGTH_SHORT).show();

    }
}

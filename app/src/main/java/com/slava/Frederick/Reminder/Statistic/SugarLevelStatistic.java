package com.slava.Frederick.Reminder.Statistic;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.blanyal.remindme.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.parse.ParseObject;
import com.slava.Frederick.Reminder.AddNewMeasurement;
import com.slava.Frederick.Reminder.Authentication.DataCollector;
import com.slava.Frederick.Reminder.MainFrederickActivity;
import com.slava.Frederick.Reminder.MapsActivity;
import com.slava.Frederick.Reminder.Reminder.DateTimeSorter;
import com.slava.Frederick.Reminder.Reminder.MainActivityReminder;
import com.slava.Frederick.Reminder.UserProfile.UserProfile;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

public class SugarLevelStatistic extends AppCompatActivity {
    // For side menu
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigation;
    private RecyclerView mList;
    private SugarTestAdapter mAdapter;
    private MultiSelector mMultiSelector = new MultiSelector();
    private LinkedHashMap<Integer, Integer> IDmap = new LinkedHashMap<>();
    List<ParseObject> tests;
    List<ParseObject> todayTests;
    DataCollector globalData;

    //toolbar
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugar_level_statistic);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        // Setup Toolbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Sugar Statistic");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        globalData = DataCollector.getInstance();
        globalData.getAllSugarTestsFromParse();
        globalData.getTodaySugarTestsFromParse();
        tests = globalData.getTests();

        initInstances();
        mList = (RecyclerView) findViewById(R.id.sugar_tests_list);

        // Create recycler view
        mList.setLayoutManager(getLayoutManager());
        registerForContextMenu(mList);
        mAdapter = new SugarTestAdapter();
        mAdapter.setItemCount(getDefaultItemCount());
        mList.setAdapter(mAdapter);

        todayTests = globalData.getTodayTests();

        //Get times from parse object and add it to array of strings
        ArrayList<String> dateString=new ArrayList<String>();
        for(ParseObject time : todayTests){
            dateString.add(time.getString("time") +"$" + time.getString("sugarLevel"));
        }

        Collections.sort(dateString, new Comparator<String>() {
            DateFormat f = new SimpleDateFormat("hh:mm");
            @Override
            public int compare(String o1, String o2) {
                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });


        LineChart lineChart = (LineChart) findViewById(R.id.chart);
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();

        int i = 0;
        for(String time : dateString){
            String[] x = time.toString().split("\\$");
            labels.add(x[0]);
            entries.add(new Entry(Integer.parseInt(x[1]), i));
            i++;
        }


        LineDataSet dataset = new LineDataSet(entries, "# of Sugar test");
        dataset.setDrawCubic(true);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        dataset.setDrawCircles(true);
        dataset.setDrawFilled(true);
        dataset.setValueTextSize(12f);

        LineData data = new LineData(labels, dataset);
        data.setValueTextSize(10f);


        lineChart.setData(data);
        lineChart.animateY(2500);
        lineChart.setBackgroundColor(Color.WHITE);
    }


    // Layout manager for recycler view
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    private void initInstances() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(SugarLevelStatistic.this, drawerLayout, R.string.hello_world, R.string.hello_world);
        drawerLayout.setDrawerListener(drawerToggle);

        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.navigation_item_1:
                        startActivity(new Intent(SugarLevelStatistic.this,MainFrederickActivity.class));
                        break;
                    case R.id.navigation_item_2:
                        startActivity(new Intent(SugarLevelStatistic.this,AddNewMeasurement.class));
                        break;
                    case R.id.navigation_item_3:
                        startActivity(new Intent(SugarLevelStatistic.this,MainActivityReminder.class));
                        break;
                    case R.id.navigation_item_4:
                        startActivity(new Intent(SugarLevelStatistic.this, UserProfile.class));
                        break;
                    case R.id.navigation_item_5:
                        startActivity(new Intent(SugarLevelStatistic.this,SugarLevelStatistic.class));
                        break;
                    case R.id.navigation_item_6:
                        startActivity(new Intent(SugarLevelStatistic.this, MapsActivity.class));
                        break;
                }
                return false;
            }
        });

    }

    // Creating the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.statistic_bar_menu, menu);
        return true;
    }

    // On clicking menu buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // On clicking the back arrow
            // Discard any changes
            case android.R.id.home:
                onBackPressed();
                return true;

            // On clicking save reminder button
            case R.id.export_statistic:
                startActivity(new Intent(SugarLevelStatistic.this, ExportDataToEmail.class));
                return true;
//
//            // On clicking discard reminder button
//            // Discard any changes
//            case R.id.drop_sugar_test:
//                RelativeLayout layout = (RelativeLayout) findViewById(R.id.sugar_level_layout_id);
//                layout.setBackgroundColor(getResources().getColor(R.color.primary_layout_background));
//
//                sugarLevel.setText("Enter Sugar Level");
//                Toast.makeText(getApplicationContext(), "Discarded", Toast.LENGTH_SHORT).show();
//                onBackPressed();
//                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected int getDefaultItemCount() {
        return 100;
    }


    // Adapter class for recycler view
    public class SugarTestAdapter extends RecyclerView.Adapter<SugarTestAdapter.VerticalItemHolder> {
        private ArrayList<SugarTestItem> mItems;

        public SugarTestAdapter() {
            mItems = new ArrayList<>();
        }

        public void setItemCount(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

//        public void onDeleteItem(int count) {
//            mItems.clear();
//            mItems.addAll(generateData(count));
//        }
//
//        public void removeItemSelected(int selected) {
//            if (mItems.isEmpty()) return;
//            mItems.remove(selected);
//            notifyItemRemoved(selected);
//        }

        // View holder for recycler view items
        @Override
        public VerticalItemHolder onCreateViewHolder(ViewGroup container, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View root = inflater.inflate(R.layout.recycle_sugat_test_items, container, false);

            return new VerticalItemHolder(root, this);
        }

        @Override
        public void onBindViewHolder(VerticalItemHolder itemHolder, int position) {
            SugarTestItem item = mItems.get(position);
            itemHolder.setSugarTestTitle(item.mTitle);
            itemHolder.setSugarTestDateTime(item.mDateTime);
            itemHolder.setSugarTestStatus(item.mStatus);
            itemHolder.setActiveImage(item.mActive);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        // Class for recycler view items
        public  class SugarTestItem {
            public String mTitle;
            public String mDateTime;
            public String mActive;
            public String mStatus;

            public SugarTestItem(String Title, String DateTime, String Active, String Status) {
                this.mTitle = Title;
                this.mDateTime = DateTime;
                this.mActive = Active;
                this.mStatus = Status;
            }
        }

        // Class to compare date and time so that items are sorted in ascending order
        public class DateTimeComparator implements Comparator {
            DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm");

            public int compare(Object a, Object b) {
                String o1 = ((DateTimeSorter)a).getDateTime();
                String o2 = ((DateTimeSorter)b).getDateTime();

                try {
                    return f.parse(o2).compareTo(f.parse(o1));
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        // UI and data class for recycler view items
        public  class VerticalItemHolder extends SwappingHolder  {
            private TextView mTitleText, mDateAndTimeText, nStatus;
            private ImageView mActiveImage , mThumbnailImage;
            private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
            private TextDrawable mDrawableBuilder;
            private SugarTestAdapter mAdapter;

            public VerticalItemHolder(View itemView, SugarTestAdapter adapter) {
                super(itemView, mMultiSelector);
//                itemView.setOnClickListener(this);
//                itemView.setOnLongClickListener(this);
                itemView.setLongClickable(true);

                // Initialize adapter for the items
                mAdapter = adapter;

                // Initialize views
                mTitleText = (TextView) itemView.findViewById(R.id.recycle_title);
                mDateAndTimeText = (TextView) itemView.findViewById(R.id.recycle_date_time);
                nStatus = (TextView) itemView.findViewById(R.id.recycle_repeat_info);
                mActiveImage = (ImageView) itemView.findViewById(R.id.active_image);
                mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
            }

            // On clicking a reminder item
//            @Override
//            public void onClick(View v) {
//                if (!mMultiSelector.tapSelection(this)) {
//                    mTempPost = mList.getChildAdapterPosition(v);
//
//                    int mReminderClickID = IDmap.get(mTempPost);
//                    selectReminder(mReminderClickID);
//
//                } else if(mMultiSelector.getSelectedPositions().isEmpty()){
//                    mAdapter.setItemCount(getDefaultItemCount());
//                }
//            }

            // On long press enter action mode with context menu
//            @Override
//            public boolean onLongClick(View v) {
//                AppCompatActivity activity = MainActivityReminder.this;
//                activity.startSupportActionMode(mDeleteMode);
//                mMultiSelector.setSelected(this, true);
//                return true;
//            }

            // Set test title view
            public void setSugarTestTitle(String title) {
                mTitleText.setText(title);
            }

            // Set date and time views
            public void setSugarTestDateTime(String datetime) {
                mDateAndTimeText.setText(datetime);
            }

            // Set repeat views
            public void setSugarTestStatus(String status) {
                nStatus.setText(status);
                String letter = "A";
                int color;

                if(status != null && !status.isEmpty()) {
                    letter = status.substring(0, 1);
                }

                if(letter.equals("L") || letter.equals("H")){
                    color = new Color().RED;
                }else{
                    color = new Color().GREEN;
                }


                // Create a circular icon consisting of  a random background colour and first letter of status
                mDrawableBuilder = TextDrawable.builder().buildRound(letter, color);
                mThumbnailImage.setImageDrawable(mDrawableBuilder);
            }

            // Set active image as on or off
            public void setActiveImage(String active){
                if(active.equals("true")){
                    mActiveImage.setImageResource(R.drawable.ic_notifications_on_white_24dp);
                }else if (active.equals("false")) {
                    mActiveImage.setImageResource(R.drawable.ic_notifications_off_grey600_24dp);
                }
            }
        }

        // Generate real data for each item
        public List<SugarTestItem> generateData(int count) {
            ArrayList<SugarTestAdapter.SugarTestItem> items = new ArrayList<>();

            // Get all reminders from the database

            // Initialize lists
            List<String> Titles = new ArrayList<>();
            List<String> Actives = new ArrayList<>();
            List<String> DateAndTime = new ArrayList<>();
            List<String> Status = new ArrayList<>();
            List<Integer> IDList= new ArrayList<>();
            List<DateTimeSorter> DateTimeSortList = new ArrayList<>();


                int j=0;
                for (ParseObject test : tests){
                    j++;
                    Titles.add("Sugar level " + test.get("sugarLevel") + " mg/l");
                    DateAndTime.add(test.get("date") + " " + test.get("time"));
                    Actives.add("");
                    Status.add(test.get("status").toString());
                    IDList.add(j);
                }

            int key = 0;

            // Add date and time as DateTimeSorter objects
            for(int k = 0; k<Titles.size(); k++){
                DateTimeSortList.add(new DateTimeSorter(key, DateAndTime.get(k)));
                key++;
            }

            // Sort items according to date and time in ascending order
            Collections.sort(DateTimeSortList, new DateTimeComparator());
            Collections.reverseOrder(new DateTimeComparator());

            int k = 0;

            // Add data to each recycler view item
            for (DateTimeSorter item:DateTimeSortList) {
                int i = item.getIndex();

                items.add(new SugarTestAdapter.SugarTestItem(Titles.get(i), DateAndTime.get(i),Actives.get(i), Status.get(i)));
                IDmap.put(k, IDList.get(i));
                k++;
            }
            return items;
        }
    }
}

package com.slava.Frederick.Reminder.NLP;

import java.util.TreeSet;

/**
 * Created by slava on 3/26/16.
 */
public class Lexicon {
    TreeSet<String> myLexicon = new TreeSet<String>();
    NumberLexicon numberLexicon;

    public Lexicon() {
        buildLexicon();
        this.numberLexicon = new NumberLexicon();
    }

    private void buildLexicon() {
        // PRONOUNS
        myLexicon.add("you");
        myLexicon.add("U");
        myLexicon.add("ya");
        myLexicon.add("I");
        myLexicon.add("me");
        myLexicon.add("he");
        myLexicon.add("she");
        myLexicon.add("it");
        myLexicon.add("we");
        myLexicon.add("they");
        myLexicon.add("him");
        myLexicon.add("her");
        myLexicon.add("us");
        myLexicon.add("them");

        //POSSESSIVE ADJECTIVES AND PRONOUNS
        myLexicon.add("my");
        myLexicon.add("mine");
        myLexicon.add("your");
        myLexicon.add("yours");
        myLexicon.add("his");
        myLexicon.add("her");
        myLexicon.add("hers");
        myLexicon.add("its");
        myLexicon.add("our");
        myLexicon.add("ours");
        myLexicon.add("their");
        myLexicon.add("theirs");

        //PREPOSITION
        myLexicon.add("aboard");
        myLexicon.add("about");
        myLexicon.add("above");
        myLexicon.add("across");
        myLexicon.add("after");
        myLexicon.add("against");
        myLexicon.add("along");
        myLexicon.add("amid");
        myLexicon.add("among");
        myLexicon.add("anti");
        myLexicon.add("around");
        myLexicon.add("as");
        myLexicon.add("at");
        myLexicon.add("before");
        myLexicon.add("behind");
        myLexicon.add("below");
        myLexicon.add("beneath");
        myLexicon.add("beside");
        myLexicon.add("besides");
        myLexicon.add("between");
        myLexicon.add("beyond");
        myLexicon.add("but");
        myLexicon.add("by");
        myLexicon.add("concerning");
        myLexicon.add("considering");
        myLexicon.add("down");
        myLexicon.add("during");
        myLexicon.add("except");
        myLexicon.add("excepting");
        myLexicon.add("excluding");
        myLexicon.add("following");
        myLexicon.add("for");
        myLexicon.add("from");
        myLexicon.add("in");
        myLexicon.add("inside");
        myLexicon.add("into");
        myLexicon.add("like");
        myLexicon.add("minus");
        myLexicon.add("near");
        myLexicon.add("of");
        myLexicon.add("off");
        myLexicon.add("on");
        myLexicon.add("onto");
        myLexicon.add("opposite");
        myLexicon.add("outside");
        myLexicon.add("over");
        myLexicon.add("past");
        myLexicon.add("per");
        myLexicon.add("plus");
        myLexicon.add("regarding");
        myLexicon.add("round");
        myLexicon.add("save");
        myLexicon.add("since");
        myLexicon.add("than");
        myLexicon.add("through");
        myLexicon.add("to");
        myLexicon.add("toward");
        myLexicon.add("towards");
        myLexicon.add("under");
        myLexicon.add("underneath");
        myLexicon.add("unlike");
        myLexicon.add("until");
        myLexicon.add("up");
        myLexicon.add("upon");
        myLexicon.add("versus");
        myLexicon.add("via");
        myLexicon.add("with");
        myLexicon.add("within");
        myLexicon.add("without");

        //NOUN
        myLexicon.add("fasting");
        myLexicon.add("meal");
        myLexicon.add("sugar");
        myLexicon.add("level");

        //VERB TO BE
        myLexicon.add("be");
        myLexicon.add("am");
        myLexicon.add("is");
        myLexicon.add("are");
        myLexicon.add("was");
        myLexicon.add("were");
        myLexicon.add("being");
        myLexicon.add("been");

        myLexicon.add("meet");
        myLexicon.add("go");
        myLexicon.add("going");
        myLexicon.add("feel");
        myLexicon.add("take");


        //Adjective
        myLexicon.add("nice");
        myLexicon.add("good");
        myLexicon.add("bad");
        myLexicon.add("dizzy");

        myLexicon.add("too");
        myLexicon.add("how");

        myLexicon.add("thank");
        myLexicon.add("hi");
        myLexicon.add("hello");
        myLexicon.add("Frederick");
        myLexicon.add("reminder");
        myLexicon.add("reminders");
        myLexicon.add("blood");
        myLexicon.add("statistic");
        myLexicon.add("statistics");
        myLexicon.add("personal");
        myLexicon.add("info");
        myLexicon.add("location");

        myLexicon.add("could");
        myLexicon.add("can");
        myLexicon.add("instruct");
        myLexicon.add("guide");
        myLexicon.add("do");
        myLexicon.add("did");
        myLexicon.add("done");
        myLexicon.add("make");
        myLexicon.add("made");
        myLexicon.add("please");








    }

    // Check if the word in lexicon
    public boolean isLexiconContainWord(String word) {
        return myLexicon.contains(word);
    }

    public boolean sanityTestOnSentence(String sentence) {
        String[] arrayOfWords = sentence.split(" ");
        for (String word : arrayOfWords) {
            if (isLexiconContainWord(word) || numberLexicon.isNumberLexiconContainWord(word))
                return true;
        }
        return false;
    }
}
